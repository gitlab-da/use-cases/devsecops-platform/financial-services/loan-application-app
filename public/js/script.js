/* Add a submit listener to document mortgageForm with fields propertyPrice, loanAmount, repaymentTime, and interestRate. Then calculate monthlyInterestRate, numberOfPayments, monthlyPayment and format them. Store the calculations into fields monthlyPayment, interestInfo, and mortgageAmount but precede them with titles. Lastly, remove the hidden of the element result.
>>> <script src="js/script.js"></script> 
*/

document.getElementById('mortgageForm').addEventListener('submit', function(event) {
    event.preventDefault();
    
    const propertyPrice = parseFloat(document.getElementById('propertyPrice').value);
    const loanAmount = parseFloat(document.getElementById('loanAmount').value);
    const repaymentTime = parseFloat(document.getElementById('repaymentTime').value);
    const interestRate = parseFloat(document.getElementById('interestRate').value);

    const monthlyInterestRate = (interestRate / 100) / 12;
    const numberOfPayments = repaymentTime * 12;

    const monthlyPayment = (loanAmount * monthlyInterestRate) / (1 - Math.pow((1 + monthlyInterestRate), -numberOfPayments));
    const formattedMonthlyPayment = monthlyPayment.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
    
    document.getElementById('monthlyPayment').innerText = `Monthly payment: ${formattedMonthlyPayment} USD`;
    document.getElementById('interestInfo').innerText = `Total Interest: ${interestRate}% p.a. with fixation for ${repaymentTime} years`;
    document.getElementById('mortgageAmount').innerText = `Mortgage Amount: USD ${loanAmount.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ' ')}`;

    document.getElementById('result').classList.remove('hidden');
});
